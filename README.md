# Final

My final project for Honors 301 (Math & Music) using Sonic PI

## Description

Final project for Math and Music using Sonic PI. Using inspiration from Start Up and the chords from Song About You,
this track is reminiscient of a video game sequence with an intro, some action
and then an outro piece (similar to a Pokemon Battle or exploring a cave). The
properties of navigating the Tonnetz with the P/L/R is also used to give it
a "moving" feel, while additive elements (with a palindromic pattern) give it an
interesting flare.

## Usage

Load code or file into Sonic PI and run the program.

## Authors and acknowledgment

Developed by Christopher S. Good, Jr.
