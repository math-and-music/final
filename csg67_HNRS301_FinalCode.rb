'''
Christopher Good (csg67)
Final Project
May 31, 2022

Theme: Using inspiration from Start Up and the chords from Song About You, 
this track is reminiscient of a video game sequence with an intro, some action 
and then an outro piece (similar to a Pokemon Battle or exploring a cave). The
properties of navigating the Tonnetz with the P/L/R is also used to give it 
a "moving" feel, while additive elements (with a palindromic pattern) give it an 
interesting flare.

Usage: The file is designed to played all at once and should handle the 
composition (perform itself) by moving sounds and in and out.
'''

# Found Sounds
xbox = "~/Music/SonicPi/xbox_start.wav"
door = "~/Music/SonicPi/door_close.wav"

# Triggers
PLAY_BEAT = false # Start with the beat off
PLAY_INTRO_MELODY = true # Start with the intro on

# Cool start sequence (Similar to Start Up by Jonathan & Friends)
sample xbox

sleep 3 # Sleep to orchestrate the order of appearance

with_fx :echo do
  live_loop :intro_melody do
    if PLAY_INTRO_MELODY # Only play intro for first 6 seconds
      play [:D, :A, :G].choose, amp: 0.5
      sleep 0.5
      play chord(:b, :minor), amp: 0.5, sustain: 1
      sleep 1
    else # Let LiveLoop sleep for the remainder
      sleep 1
    end
  end
end

sleep 6 # Sleep to orchestrate the order of appearance

PLAY_INTRO_MELODY = false # Stop the intro
PLAY_BEAT = true # Start the beat

live_loop :rhythm do
  if PLAY_BEAT # Orchestrate the beat when needed
    sample :elec_flip
    sleep 0.5
    sample :drum_cymbal_closed, sustain: 0.5
    sleep 0.5
  else # Let LiveLoop sleep when beat is not needed
    sleep 1
  end
end

live_loop :harmony do
  if PLAY_BEAT
    # Incremental order for fun/The Band CAMINO
    sample door, sustain: 1, amp: 2, pitch: [3, 4].choose
    sleep 0.5
  else
    sleep 0.5
  end
end

# Inversion function to help navigate the Tonnetz
define :inv do |chr, axis = 7|
  len = chr.length
  new = []
  len.times do |i|
    new[i] = ((-chr[i] + axis) % 12) + 60
  end
  return new
end

define :Parallel do |chr|
  return inv(chr, chr[0] + chr[2])
end


define :Relative do |chr|
  return inv(chr, chr[1] + chr[2])
end


define :Leading do |chr|
  return inv(chr, chr[0] + chr[1])
end

# Starting chord for traversal
init = chord(:a, :major)

# Palindromic Patterns for Synthesis
v1 = [9, 7, 5, 9, 5, 7, 9]
v2 = [9, 11, 14, 14, 11, 14, 9]

# First loop through Tonnetz
14.times do |i|
  init = [Parallel(init), Relative(init), Leading(init)].choose
  notes = [*init.map{ |note| note + v1[i % 7]}, *init.map{ |note| note + v2[i % 7] }]
  play notes, attack: 0.25, decay: 0.25, sustain: 0.5, release: 0
  sleep 1
end

PLAY_BEAT = false # Stop the beat
PLAY_INTRO_MELODY = true # Start the intro

sleep 5

PLAY_INTRO_MELODY = false # Stop the intro
PLAY_BEAT = true # Start the beat

# New starting chord for traversal
init = chord(:d, :major)

# Second loop through Tonnetz
with_fx :compressor do
  28.times do |i|
    init = [Parallel(init), Relative(init), Leading(init)].choose
    notes = [*init.map{ |note| note + v1[i % 7]}, *init.map{ |note| note + v2[i % 7] }]
    play notes, attack: 0.25, decay: 0.25, sustain: 0.5, release: 0
    sleep 1
  end
end

init = chord(:g, :major)

14.times do |i|
  init = [Parallel(init), Relative(init), Leading(init)].choose
  notes = [*init.map{ |note| note + v1[i % 7]}, *init.map{ |note| note + v2[i % 7] }]
  play notes, attack: 0.25, decay: 0.25, sustain: 0.5, release: 0
  sleep 1
end

PLAY_BEAT = false # Stop the beat
PLAY_INTRO_MELODY = true # Start the intro

sleep 6

PLAY_INTRO_MELODY = false # Stop the intro

play [:A, :G, :D], sustain: 4 # Final Note (Holding for effect)


